﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudesHTTP.Clases
{
    public class Post
    {
        static readonly string _url = "https://jsonplaceholder.typicode.com/posts";
        static readonly string _doc = "";
        public static void PostHttp()
        {
            var HttpResponse = Task.Run(async () =>
            {
                /*JObject Objdata = new JObject();
                Objdata["Id"] = 54;
                Objdata["title"] = "qqqqq";*/
                var Objdata = new
                {
                    id = 5,
                    title = "qqqq"
                };

                string result = "";
                string data = JsonConvert.SerializeObject(Objdata); 
                HttpClient client = new HttpClient();
                HttpContent content = new StringContent(data,
                                                        System.Text.Encoding.UTF8,
                                                        "application/json");
                var respuestaHttp = await client.PostAsync(_url, content);
                if (respuestaHttp.IsSuccessStatusCode)
                {
                    result = await respuestaHttp.Content.ReadAsStringAsync();
                }
                else
                {
                    result = "FALSE";
                }
                return result;
            });
            HttpResponse.Wait();
            Console.WriteLine(HttpResponse.Result);
        }

    }
}
