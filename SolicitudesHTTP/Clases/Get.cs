﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SolicitudesHTTP.Clases
{
    public class Get
    {
        static readonly string _url = "https://jsonplaceholder.typicode.com/";
        static readonly string _Doc = "https://www.youtube.com/watch?v=FKIe-1fUS6g&ab_channel=hdeleon.net";
        public static void SolicitarHttpGet()
        {
            HttpClient client = new HttpClient();
            var ResultadoHTTP = "";
            var HttpResponse =Task.Run(async ()=>
            {
                var Response = await client.GetAsync(_url+"posts");
                if (Response.IsSuccessStatusCode)
                {
                    ResultadoHTTP = await Response.Content.ReadAsStringAsync();
                }
                else
                {
                    ResultadoHTTP = "FALSE";
                }
                return ResultadoHTTP;
            });
            HttpResponse.Wait();
            Console.WriteLine(HttpResponse.Result);



            
            
        }
    }
}
